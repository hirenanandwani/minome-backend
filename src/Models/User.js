const mongoose = require('mongoose');
const {
  name,
  email,
  password,
  currency,
  isEmailVerified,
  serviceType,
  passwordChangedAt,
  passwordResetExpires,
  currentBalance,
  passwordResetToken,
} = require('./Types/CommonTypes');

const { type, paymentHistory, purchaseHistory } = require('./Types/UserTypes');
const AuthFunctions = require('./Utils/AuthFunctions');

const UserSchema = new mongoose.Schema(
  {
    name: name,
    email: email,
    password: password,
    currency: currency,
    currentBalance: currentBalance,
    isEmailVerified: isEmailVerified,
    type: type,
    paymentHistory: paymentHistory,
    serviceType: serviceType,
    passwordChangedAt: passwordChangedAt,
    passwordResetToken: passwordResetToken,
    passwordResetExpires: passwordResetExpires,
    purchaseHistory: purchaseHistory,
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);
UserSchema.pre('save', AuthFunctions.encryptPassword);
UserSchema.pre('save', AuthFunctions.updatePasswordChangeTime);

UserSchema.virtual('publishers').get(function () {
  const publisher = {};
  this.purchaseHistory.forEach((order) => {
    if (publisher[order.publisherId]) {
      publisher[order.publisherId] += 1;
    } else {
      publisher[order.publisherId] = 1;
    }
  });

  return publisher;
});

UserSchema.virtual('purchasedArticles').get(function () {
  const purchasedArticles = {};
  this.purchaseHistory.forEach((order) => {
    if (!purchasedArticles[order.articleId]) {
      purchasedArticles[order.articleId] = true;
    }
  });
  return purchasedArticles;
});

UserSchema.methods.checkPassword = async function (
  candidatePassword,
  userPassword
) {
  return AuthFunctions.checkPassword(candidatePassword, userPassword);
};

UserSchema.methods.createPasswordResetToken = function () {
  return AuthFunctions.createPasswordResetToken(this);
};

const User = mongoose.model('User', UserSchema);

module.exports = User;
