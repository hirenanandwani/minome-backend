const mongoose = require('mongoose');
const {
  name,
  email,
  password,
  currency,
  isEmailVerified,
  serviceType,
  passwordChangedAt,
  passwordResetExpires,
  passwordResetToken,
  currentBalance,
} = require('./Types/CommonTypes');
const AuthFunctions = require('./Utils/AuthFunctions');
const {
  pricePerArticle,
  numberOfFreeArticles,
  type,
  domain,
  bankName,
  bankIfsc,
  accountType,
  accountNumber,
  soldHistory,
} = require('./Types/PublisherTypes');
const PublisherSchema = new mongoose.Schema({
  name: name,
  email: email,
  password: password,
  pricePerArticle: pricePerArticle,
  numberOfFreeArticles: numberOfFreeArticles,
  currency: currency,
  isEmailVerified: isEmailVerified,
  domain: domain,
  type: type,
  serviceType: serviceType,
  passwordChangedAt: passwordChangedAt,
  passwordResetToken: passwordResetToken,
  passwordResetExpires: passwordResetExpires,
  bankName: bankName,
  bankIfsc: bankIfsc,
  accountType: accountType,
  accountNumber: accountNumber,
  soldHistory: soldHistory,
  currentBalance: currentBalance,
});

PublisherSchema.pre('save', AuthFunctions.encryptPassword);
PublisherSchema.pre('save', AuthFunctions.updatePasswordChangeTime);
PublisherSchema.methods.checkPassword = async function (
  candidatePassword,
  userPassword
) {
  return AuthFunctions.checkPassword(candidatePassword, userPassword);
};

PublisherSchema.methods.createPasswordResetToken = function () {
  return AuthFunctions.createPasswordResetToken(this);
};

const Publisher = mongoose.model('Publisher', PublisherSchema);

module.exports = Publisher;
