const bcrypt = require('bcryptjs');
const crypto = require('crypto');

async function encryptPassword(next) {
  if (!this.isModified('password')) return next();

  this.password = await bcrypt.hash(this.password, 12);
  next();
}

async function updatePasswordChangeTime(next) {
  if (!this.isModified('password') || this.isNew) return next();
  this.passwordChangedAt = Date.now() - 1000;
  next();
}

async function checkPassword(candidatePassword, userPassword) {
  return await bcrypt.compare(candidatePassword, userPassword);
}

function createPasswordResetToken(User) {
  const resetToken = crypto.randomBytes(32).toString('hex');

  User.passwordResetToken = crypto
    .createHash('sha256')
    .update(resetToken)
    .digest('hex');

  User.passwordResetExpires = Date.now() + 10 * 60 * 1000;
  return resetToken;
}

module.exports = {
  encryptPassword,
  checkPassword,
  updatePasswordChangeTime,
  createPasswordResetToken,
};
