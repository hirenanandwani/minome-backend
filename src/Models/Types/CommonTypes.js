const validator = require('validator');

exports.name = {
  type: 'String',
  trim: true,
};

exports.email = {
  type: String,
  required: [true, 'Please provide your email'],
  unique: true,
  lowercase: true,
};

exports.password = {
  type: String,
  minlength: 8,
  select: false,
};

exports.currency = {
  type: String,
};

exports.isEmailVerified = {
  type: Boolean,
  default: false,
};

exports.serviceType = {
  type: String,
  enum: ['Email', 'Google', 'Facebook'],
  default: 'Email',
};

exports.passwordChangedAt = {
  type: Date,
};
exports.passwordResetToken = {
  type: String,
};

exports.passwordResetExpires = {
  type: Date,
};
exports.currentBalance = {
  type: Number,
  default: 0,
};
