const validator = require('validator');
const mongoose = require('mongoose');

const soldArticleType = {
  date: { type: Date, default: Date.now() },
  userId: { type: mongoose.Schema.ObjectId },
  articlePrice: { type: Number },
  articleId: { type: String },
  isFree: { type: Boolean },
};

exports.pricePerArticle = {
  type: Number,
};

exports.numberOfFreeArticles = {
  type: Number,
  default: 0,
};

exports.type = {
  type: String,
  enum: ['Publisher'],
  default: 'Publisher',
};

exports.domain = {
  type: String,
  unique: true,
  default: '',
};

exports.bankName = {
  type: String,
};

exports.bankIfsc = {
  type: String,
};

exports.accountType = {
  type: String,
  enum: ['savings', 'current'],
};

exports.accountNumber = {
  type: String,
};

exports.soldHistory = {
  type: [soldArticleType],
};
