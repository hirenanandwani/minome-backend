const mongoose = require('mongoose');

const paymentObject = {
  paymentId: { type: String },
  amount: { type: String },
  date: { type: Date, default: Date.now() },
  onTheNameOf: { type: String },
};

const purchaseHistoryObject = {
  articlePrice: { type: Number },
  domain: { type: String },
  date: { type: Date, default: Date.now() },
  publisherId: { type: mongoose.Schema.ObjectId, ref: 'Publishers' },
  articleId: { type: String },
  isFree: { type: Boolean },
};

exports.type = {
  type: String,
  enum: ['User'],
  default: 'User',
};

exports.paymentHistory = {
  type: [paymentObject],
};

exports.purchaseHistory = {
  type: [purchaseHistoryObject],
};
