const AuthController = require('../Controllers/AuthController');
const FactoryController = require('../Controllers/FactoryController');
const Publisher = require('../Models/Publisher');

exports.createPublisher = AuthController.createAccount(Publisher);
exports.loginPublisher = AuthController.login(Publisher);
exports.verifyEmail = FactoryController.verifyMyEmail(Publisher);
exports.deleteMe = FactoryController.deleteMe(Publisher);
exports.updateMe = FactoryController.updateMe(Publisher);
exports.resendVerification = FactoryController.resendVerification(Publisher);
exports.serviceLogin = FactoryController.serviceLogin();
exports.getMe = FactoryController.getMyAccount();
exports.sendResetPasswordMail =
  AuthController.sendResetPasswordToken(Publisher);
exports.resetPassword = AuthController.resetPassword(Publisher);
