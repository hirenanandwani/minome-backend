const catchAsync = require('./../Utils/catchAsync');
const AppError = require('../Utils/AppError');
const jwt = require('jsonwebtoken');
const Email = require('../Utils/Email');
const crypto = require('crypto');
const signToken = (id) => {
  return jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN,
  });
};

const sendJWTToken = (user, statusCode, res) => {
  const token = signToken(user._id);
  user.password = undefined;
  res.status(statusCode).json({
    status: 'Success',
    token,
    data: {
      user,
    },
  });
};

exports.createAccount = (Model) =>
  catchAsync(async (req, res, next) => {
    if (req.body.externalService && !req.body.serviceType) {
      return next(new AppError('Please provide a Service Type', 400));
    }

    if (!req.body.externalService && !req.body.password) {
      return next(new AppError('Please Provide a Password', 400));
    }

    if (req.body.externalService && !req.body.name) {
      return next(new AppError('Please provide a Name', 400));
    }

    if (req.body.externalService && req.body.token) {
      const user = await Model.create({ ...req.body, isEmailVerified: true });

      return res.status(200).json({
        status: 'success',
        token: req.body.token,
        data: {
          user,
        },
      });
    }

    if (req.body.externalService && !req.body.token) {
      return next(new AppError('Please Provide a valid JWT', 400));
    }

    const user = await Model.create(req.body);
    const url = `${req.protocol}://${req.get(
      'host'
    )}/api/v1/${user.type.toLowerCase()}s/${user._id}/verify-email`;
    await new Email(req.body.email, url).send('welcome');
    sendJWTToken(user, 201, res);
  });

exports.login = (Model) =>
  catchAsync(async (req, res, next) => {
    const { email, password } = req.body;
    const userByEmail = await Model.findOne({ email: email });

    if (
      userByEmail &&
      (userByEmail.serviceType === 'Google' ||
        userByEmail.serviceType === 'Facebook')
    ) {
      return next(new AppError('Please try again with Google/Facebook', 401));
    }

    if (!email || !password) {
      return next(new AppError('Please Provide Proper Credentials', 400));
    }

    const user = await Model.findOne({ email }).select('+password');

    if (!user || !(await user.checkPassword(password, user.password))) {
      return next(new AppError('Incorrect Email or Password ', 401));
    }

    sendJWTToken(user, 200, res);
  });

exports.sendResetPasswordToken = (Model) =>
  catchAsync(async (req, res, next) => {
    const user = await Model.findOne({ email: req.body.email });
    if (!user) {
      return next(new AppError("User doesn't exists", 404));
    }
    if (user.serviceType !== 'Email') {
      return next(new AppError('User was not created using Email', 403));
    }
    const resetToken = user.createPasswordResetToken();
    await user.save({ validateBeforeSave: false });

    try {
      const resetURL = `https://minomoe.netlify.app/${user.type.toLowerCase()}/reset-password?token=${resetToken}`;
      await new Email(user.email, resetURL).send('forgotPassword');

      res.status(200).json({
        status: 'success',
        message: 'Token Sent to email',
      });
    } catch (error) {
      user.passwordResetToken = undefined;
      user.passwordResetExpires = undefined;
      await user.save({ validateBeforeSave: false });

      return next(
        new AppError('There was error sending email.Try again later', 500)
      );
    }
  });

exports.resetPassword = (Model) =>
  catchAsync(async (req, res, next) => {
    const hashedToken = crypto
      .createHash('sha256')
      .update(req.params.token)
      .digest('hex');
    const user = await Model.findOne({
      passwordResetToken: hashedToken,
      passwordResetExpires: { $gt: Date.now() },
    });

    if (!user) {
      return next(new AppError('Token is Invalid or has expired', 401));
    }

    if (!req.body.password) {
      return next(new AppError('Please Provide a Password', 404));
    }

    user.password = req.body.password;
    user.passwordResetToken = undefined;
    user.passwordResetExpires = undefined;

    await user.save();
    sendJWTToken(user, 200, res);
  });
