const catchAsync = require('./../Utils/catchAsync');
const AppError = require('../Utils/AppError');
const Email = require('../Utils/Email');

exports.getMyAccount = () =>
  catchAsync(async (req, res, next) => {
    res.status(200).json({
      data: req.user,
    });
  });

exports.updateMe = (Model) =>
  catchAsync(async (req, res, next) => {
    const {
      user,
      user: { id },
    } = req;
    if (!user) {
      return next(new AppError("User doesn't exist", 401));
    }
    const updatedUser = await Model.findByIdAndUpdate(id, req.body, {
      new: true,
      runValidators: true,
    });

    res.status(200).json({
      status: 'success',
      data: updatedUser,
    });
  });

exports.deleteMe = (Model) =>
  catchAsync(async (req, res, next) => {
    const user = await Model.findByIdAndDelete(req.user._id);
    if (!user) {
      return next(new AppError('No User found', 404));
    }
    res.status(204).json({
      status: 'success',
      data: null,
    });
  });

exports.serviceLogin = () =>
  catchAsync(async (req, res, next) => {
    if (!req.body.serviceType) {
      return next(new AppError('Please Provide a valid service type', 400));
    }
    if (req.body.serviceType !== req.user.serviceType) {
      return next(
        new AppError("The Credentials doesn't match. Please Try again", 401)
      );
    }

    res.status(200).json({
      status: 'success',
      token: req.token,
      data: {
        user: req.user,
      },
    });
  });

exports.verifyMyEmail = (Model) =>
  catchAsync(async (req, res, next) => {
    const user = await Model.findByIdAndUpdate(
      req.params.id,
      {
        isEmailVerified: true,
      },
      {
        new: true,
      }
    );
    if (!user) {
      return next(new AppError("User doesn't exists ", 401));
    }
    res.status(200).json({
      status: 'success',
      data: user,
    });
  });

exports.resendVerification = (Model) =>
  catchAsync(async (req, res, next) => {
    const user = await Model.findOne({ email: req.body.email });
    if (!user) {
      return next(new AppError("User doesn't exists", 401));
    }

    if (user.isEmailVerified) {
      return next(new AppError('Email already Verified', 403));
    }
    const url = `${req.protocol}://${req.get(
      'host'
    )}/api/v1/${user.type.toLowerCase()}s/${user._id}/verify-email`;
    await new Email(user.email, url).send('welcome');
    res.send('Email sent');
  });
