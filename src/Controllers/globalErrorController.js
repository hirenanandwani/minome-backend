const AppError = require('./../Utils/AppError');

const handelCastErrorDB = (err) => {
  const message = `Invalid ${err.path}: ${err.value}`;
  return new AppError(message, 400);
};

const handelDuplicateFieldsDB = (err, res) => {
  const field = err.message.match(
    /(\w+)\s*:\s*('[^']*'|"[^"]*"|[+\-]?\d+(?:.\d+)?)/
  );

  const value = err.message.match(/(["'])(\\?.)*?\1/)[0];
  const message = `Duplicate field value: ${value} Please use another Value`;
  if (field[1]) {
    if (field[1] === 'email') {
      return new AppError(message, 400, 'Email');
    }
  }
  return new AppError(message, 400);
};

const handelValidationErrorDB = (err) => {
  const errors = Object.values(err.errors).map((el) => el.message);

  const message = `Invalid input data. ${errors.join('. ')} `;
  return new AppError(message, 400);
};

const handelJWTError = (err) =>
  new AppError('Invalid Token please login again', 401);

const handelJWTExpiredError = (err) =>
  new AppError('Your token has expired please log in again', 401);

const sendErrorDev = (err, req, res) => {
  if (req.originalUrl.startsWith('/api')) {
    return res.status(err.statusCode).json({
      status: err.status,
      error: err,
      message: err.message,
      stack: err.stack,
    });
  }

  return res.status(err.statusCode).json({
    title: 'Something Went wrong',
    msg: err.message,
  });
};

const sendErrorProd = (err, req, res) => {
  if (req.originalUrl.startsWith('/api')) {
    if (err.isOperational) {
      return res.status(err.statusCode).json({
        status: err.status,
        message: err.message,
        errorCode: err.errorCode,
        verified: req.existingUser ? req.existingUser.isEmailVerified : null,
      });
    }

    return res.status(500).json({
      status: 'error',
      message: 'Something went very wrong',
    });
  }

  if (err.isOperational) {
    return res.status(err.statusCode).json({
      title: 'Something Went wrong',
      msg: err.message,
      errorCode: err.errorCode,
      verified: req.existingUser ? req.existingUser.isEmailVerified : null,
    });
  }
  return res.status(err.statusCode).json({
    title: 'Something Went wrong',
    msg: 'Please try again later',
  });
};

module.exports = (err, req, res, next) => {
  err.statusCode = err.statusCode || 500;
  if (process.env.NODE_ENV === 'development') {
    sendErrorDev(err, req, res);
  } else if (process.env.NODE_ENV === 'production') {
    let error = { ...err };

    error.message = err.message;

    if (error.name === 'CastError') error = handelCastErrorDB(error);
    if (error.code === 11000) error = handelDuplicateFieldsDB(error, res);
    if (error.name === 'ValidationError')
      error = handelValidationErrorDB(error);
    if (error.name === 'JsonWebTokenError') error = handelJWTError(error);
    if (error.name === 'TokenExpiredError')
      error = handelJWTExpiredError(error);
    sendErrorProd(error, req, res);
  }
};
