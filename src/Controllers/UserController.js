const AuthController = require('../Controllers/AuthController');
const FactoryController = require('../Controllers/FactoryController');
const Publisher = require('../Models/Publisher');
const User = require('../Models/User');
const AppError = require('../Utils/AppError');
const catchAsync = require('../Utils/catchAsync');

exports.createUser = AuthController.createAccount(User);
exports.loginUser = AuthController.login(User);
exports.verifyEmail = FactoryController.verifyMyEmail(User);
exports.updateMe = FactoryController.updateMe(User);
exports.deleteMe = FactoryController.deleteMe(User);
exports.resendVerification = FactoryController.resendVerification(User);
exports.sendResetPasswordMail = AuthController.sendResetPasswordToken(User);
exports.resetPassword = AuthController.resetPassword(User);
exports.serviceLogin = FactoryController.serviceLogin();
exports.getMe = FactoryController.getMyAccount();
