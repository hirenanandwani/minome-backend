const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const app = express();
const { wipeData } = require('./Utils/WipeData');
const AppError = require('./Utils/AppError');
const UserRouter = require('./Routes/UserRoutes');
const PublisherRouter = require('./Routes/PublisherRoutes');
const globalErrorController = require('./Controllers/globalErrorController');

if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

app.use(cors());
app.use('/api/v1/users/payment-success', express.raw({ type: '*/*' }));
app.use(express.json());

//Remove In production
app.get('/api/v1', (req, res, next) => {
  if (req.query.key !== process.env.PETER_KEY) {
    return next(new AppError("Don't try to be peter", 401));
  }
  wipeData();
  res.send('<h1>Hello Peter, You destroyed the Database</h1>');
});
//Remove In production

app.use('/api/v1/users', UserRouter);
app.use('/api/v1/publishers', PublisherRouter);

app.all('*', (req, res, next) => {
  next(new AppError(`Can't find ${req.originalUrl} on this server`, 404));
});

app.use(globalErrorController);

module.exports = app;
