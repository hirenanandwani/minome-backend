const { promisify } = require('util');
const catchAsync = require('../Utils/catchAsync');
const AppError = require('../Utils/AppError');
const jwt = require('jsonwebtoken');
const User = require('../Models/User');
const checkAuth = (Model) =>
  catchAsync(async (req, res, next) => {
    let token;
    if (
      req.headers.authorization &&
      req.headers.authorization.startsWith('Bearer')
    ) {
      token = req.headers.authorization.split(' ')[1];
    }

    if (!token) {
      return next(
        new AppError('You are not logged in! Please Login to get access', 401)
      );
    }

    // const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET);
    const decoded = jwt.decode(token);
    if (decoded.exp < Math.ceil(Date.now() / 1000)) {
      return next(new AppError('Your token expired', 401));
    }
    let currentUser;
    if (decoded.id) {
      currentUser = await Model.findById(decoded.id);
    } else if (decoded.email) {
      currentUser = await Model.findOne({ email: decoded.email });
    }
    if (!currentUser) {
      next(
        new AppError('The User belonging to the token no longer exist', 401)
      );
    }
    req.user = currentUser;
    res.locals.user = currentUser;
    req.token = token;
    next();
  });

module.exports = checkAuth;
