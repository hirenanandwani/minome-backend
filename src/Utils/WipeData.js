const Publisher = require('../Models/Publisher');
const User = require('../Models/User');

exports.wipeData = () => {
  User.collection.drop();
  Publisher.collection.drop();
};
