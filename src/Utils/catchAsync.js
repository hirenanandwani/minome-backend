const User = require('../Models/User');
const Publisher = require('../Models/Publisher');
module.exports = (fn) => {
  return async (req, res, next) => {
    if (req.baseUrl === '/api/v1/users' && req._parsedUrl.path === '/signup') {
      const user = await User.findOne({ email: req.body.email });
      if (user) {
        req.existingUser = user;
      }
    }
    if (
      req.baseUrl === '/api/v1/publishers' &&
      req._parsedUrl.path === '/signup'
    ) {
      const user = await Publisher.findOne({ email: req.body.email });
      if (user) {
        req.existingUser = user;
      }
    }
    fn(req, res, next).catch(next);
  };
};
