const nodemailer = require('nodemailer');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(
  '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'
);

const templates = {
  welcome: 'd-fc3815dde00c43878046bf73999109ca',
  forgotPassword: 'd-3c66172c1c1a4c269fadb35cd6ba69f0',
};

class Email {
  constructor(email, url) {
    this.email = email;
    this.url = url;
  }

  async send(template) {
    const options = {
      from: 'minomoe.content@gmail.com',
      to: this.email,
      subject: 'Test with template',
      template_id: templates[template],
      dynamic_template_data: {
        external_url: this.url,
      },
    };

    try {
      await sgMail.send(options);
    } catch (error) {
      console.log(error.response.body);
    }
  }
}

module.exports = Email;
