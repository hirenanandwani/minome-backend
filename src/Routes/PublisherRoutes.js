const express = require('express');
const PublisherController = require('../Controllers/PublisherController');
const checkAuth = require('../Middlewares/AuthMiddleware');
const Publisher = require('../Models/Publisher');

const router = express.Router();
router.route('/signup').post(PublisherController.createPublisher);
router.route('/login').post(PublisherController.loginPublisher);
router.route('/:id/verify-email').get(PublisherController.verifyEmail);
router
  .route('/resend-verification')
  .post(PublisherController.resendVerification);
router.route('/reset-password').post(PublisherController.sendResetPasswordMail);
router.route('/reset-password/:token').post(PublisherController.resetPassword);

router.use(checkAuth(Publisher));
router
  .route('/me')
  .get(PublisherController.getMe)
  .patch(PublisherController.updateMe)
  .delete(PublisherController.deleteMe);

router.route('/service-login').post(PublisherController.serviceLogin);

module.exports = router;
