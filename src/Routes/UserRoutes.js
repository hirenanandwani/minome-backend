const express = require('express');
const UserController = require('../Controllers/UserController');
const checkAuth = require('../Middlewares/AuthMiddleware');
const User = require('../Models/User');

const router = express.Router();

router.route('/signup').post(UserController.createUser);
router.route('/login').post(UserController.loginUser);
router.route('/:id/verify-email').get(UserController.verifyEmail);
router.route('/resend-verification').post(UserController.resendVerification);
router.route('/reset-password').post(UserController.sendResetPasswordMail);
router.route('/reset-password/:token').post(UserController.resetPassword);

router.use(checkAuth(User));

router
  .route('/me')
  .get(UserController.getMe)
  .patch(UserController.updateMe)
  .delete(UserController.deleteMe);

router.route('/service-login').post(UserController.serviceLogin);

module.exports = router;
